## Установка и настройка OpenShift и IBM Cloud Pak for Integration

### Введение

Для начала кратко сформулируем нашу задачу: у нас есть "голые" сервера (пока не очень важно, физические или виртуальные), а мы хотим развернуть некий софт от IBM из комплекта Cloud Pak for Integration. Причем развернуть хотим именно в контейнерах! Это важная оговорка, так как некоторые из решений Cloud Pak for Integration (MQ, DataPower, например) до сих пор допускают самостоятельное развертывание без всей этой мишуры.

Краткая предыстория для тех, кто не в курсе. Многие (вероятно, большинство) software продуктов от IBM переходит на новую модель дистрибуции/развертывания. Это частное или публичное облако, предоставляющее платформу RedHat OpenShift (при этом помним, что некоторые продукты по-прежнему поддерживают развертывание "по старинке", см. предыдущий абзац). IBM предлагает некоторое количество паков для OpenShift, которые ставятся поверх этой платформы, и далее дают возможность установить искомые продукты.

 Текущий набор этих паков следующий:

1. for Integration (MQ, App Connect (бывший Integration Bus), DataPower, API Connect...)
2. for Applications (WebSphere Application Server, Liberty...)
3. for Automation (Operation Decision Manager, FileNet P8 Content Platform Engine...)
4. for Data (DB2, DataStage, что-то из аналитики - Cognos, SPSS...)
5. for Security (работает как единая точка для приема и аналитики по данным от разных систем безопасности а-ля QRadar)
6. for Multicloud Management (Monitoring, Terraform...)

Состав IBM (и не только) продуктов для каждого пака указан навскидку, поэтому, конечно же, не претендует на полноту (на что намекают мои многоточия). Если есть желание, всегда можно посмотреть на сайте IBM. Хотя это и не самая тривиальная задача :) При этом нужно помнить, что помимо основных продуктов, там есть различные вспомогательные инструменты, которые тоже попадают в состав этих паков. CI инструменты для наших Applications, мониторинговые инструменты для оценки загруженности нашей Integration и т.п.

 OpenShift построен на основе Kubernetes, отсюда вывод: если кто-то хочет научиться все это поднимать и настраивать, нужно уметь администрировать Docker + Kubernetes + OpenShift + Cloud Pak. Но вы же любите сложные задачи? Если вдруг бэкграунд по Docker/Kubernetes слабый, можно подтянуть: у нашего учебного центра была серия бесплатных вебинаров.

Docker: https://youtu.be/cRAg9Oonir0

Kubernetes: https://youtu.be/m6LM06kRywg

OpenShift: https://youtu.be/5epAull_WK8

Если тяжело пойдет, можно и на курсы наши посмотреть на http://qdts.ru по этим темам.

Если мы говорим про локальное развертывание всего этого великолепия у себя, нам нужно будет решить следующие практические задачи:

1. Поднять OpenShift
2. Установить Cloud Pak for ...
3. Развернуть инстансы нужного софта из этого Cloud Pak
4. Как-то дальше жить с этим софтом: настраивать, мониторить, логгировать, настраивать безопасность и т.п.

По логике вещей вопросы 2, 3, 4 должны решаться довольно однообразно для разных Cloud Pak. Но мой, пока еще довольно скромный, опыт говорит, что нюансов там весьма много для каждого из Cloud Pak. Более того, нюансы есть даже по первому пункту. Но обо всем по порядку. Тем не менее, сразу же замечу, что необъятных задач по всем пакам я в рамках этой статьи не ставлю - сосредоточусь на Integration.

Небольшой дисклеймер: я добавил в статью некоторое количество ссылок на материалы от RedHat и IBM. Не исключено, что к моменту прочтения вами этой статьи они слегка протухнут - все течет, все меняется, особенно на сайте IBM :) В этом случае придется немного погуглить.

### Установка OpenShift

#### Выбор версии

Сразу нужно выбрать версию OpenShift, которую мы хотим. И тут первый подвох, о котором я уже упомянул чуть раньше. Сразу оговорюсь, что все нижеописанное актуально на момент написания статьи (май 2020). Разные Cloud Pak имеют разные требования по версии OpenShift. Почти все Cloud Pak хотят 4 (или есть альтернатива 3/4) версию OpenShift. Но не все! Привет Data, который хочет только 3, по крайней мере, по официальной документации (новую версию обещают уже на 4.3). Но даже у тех, кто хочет/может 4 версию, могут быть разные соображения по конкретной версии: 4.2 vs 4.3 и т.п. Мы берем на мушку OpenShift 4.3, который подойдет для нашего пака.

#### Документация

https://docs.openshift.com/container-platform/4.3/installing/installing_bare_metal/installing-bare-metal.html

По ссылке выше доки, где относительно подробно описан процесс установки. Выбрана версия 4.3 и вариант установки Bare Metal. То есть, на голом железе без виртуализации. Или как будто бы без виртуализации! На самом деле, в моем случае я буду ставить на vSphere ESXi виртуалках. Вы можете возразить, что есть вариант специально для vSphere и почему бы не воспользоваться им? Да, действительно было бы уместно. И мы бы сэкономили немного времени (позже покажу как), но у меня есть две проблемы на этот счет:

1. я - не администратор своей инфраструктуры ESXi и у меня нет прав на создание/изменение настроек виртуалок (решаемо)
2.  от меня хотят версию vSphere 6.5, а у меня 6.0. Не знаю, насколько это критично, но для меня это стало сигналом к тому, что лучше пойти по более хардкорному пути (Bare Metal)

#### Скачивание дистрибутива

Этот и последующие разделы содержат по сути краткий пересказ статьи из документации, сдобренный моими комментариями и пояснениями, которых там (в доках) явно не хватает. Так что, если хотите пройти по моему пути, одним глазом придется смотреть сюда, а другим на первоисточник!

Так вот, дистрибутивы! Нужно попасть сюда https://cloud.redhat.com/openshift/install. Там спросят регистрацию на сайте RedHat. Если вдруг ее нет, нужно зарегистрироваться.

Выбираем подходящий вариант, в моем случае Run on Bare Metal. После чего еще за пару кликов мы доберемся до 5ти файлов. Запускать установку мы будем с машины, где установлен Linux, так что в тех случаях, когда нам предлагают альтернативу, везде выбираем Linux. Итак:

| Название                                                     | Файл                                    | Ссылка                                                       | Описание                                                     |
| ------------------------------------------------------------ | --------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| OpenShift installer                                          | openshift-install-linux.tar.gz          | https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.3.19/openshift-install-linux.tar.gz | Установщик                                                   |
| Pull secret                                                  | pull-secret.txt                         | ---                                                          | Секрет, который использует установщик                        |
| Red Hat Enterprise Linux CoreOS (RHCOS) - ISO                | rhcos-4.3.8-x86_64-installer.x86_64.iso | https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/latest/latest/rhcos-4.3.8-x86_64-installer.x86_64.iso | Образ RHCOS в формате ISO                                    |
| Red Hat Enterprise Linux CoreOS (RHCOS) - для сетевой установки | rhcos-4.3.8-x86_64-metal.x86_64.raw.gz  | https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/4.3/latest/rhcos-4.3.8-x86_64-metal.x86_64.raw.gz | RHCOS дистрибутив, который будет использован во время установки (там есть для два варианта для BIOS и UEFI, в зависимости от настроек железок/виртуалок - у меня BIOS) |
| Command-line interface                                       | openshift-client-linux.tar.gz           | https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.3.19/openshift-client-linux-4.3.19.tar.gz | Утилиты для управления кластером                             |

Чисто теоретически, может понадобиться что-то еще, но не в моем случае. Про остальные случаи чуть позже. Держите в уме, что вам, возможно, придется скачать что-то другое с учетом вашей версии (довольно часто выходят обновления) и вашей платформы виртуализации/облачного провайдера.

#### Подготовка и пререквизиты

##### Виртуальные машины

Нам нужно подготовить несколько виртуальных машин. Вот здесь и скажется тот факт, что я использую Bare Metal, а не vSphere сценарий установки. В первом случае придется прописывать параметры загрузки для всех машин, которые будут входить в кластер. Во втором же жизнь немного упростится, так как мы сможем положиться на административные интерфейсы vSphere для решения этих вопросов, но вы уже поняли, что я так не делал, поэтому умничать не буду :)

Всего нам будет нужно 6 виртуальных машин для минимальной (!) конфигурации. Чтобы не повторять документацию, сразу добавлю в табличку свои рекомендации по ресурсам для этих машин:

| Имя          | Роль                       | ОС              | Минимум ресурсов               | Рекомендуемо ресурсов              |
| ------------ | -------------------------- | --------------- | ------------------------------ | ---------------------------------- |
| OS_Bootstrap | для начальной настройки    | RHCOS           | 4vCPU, 16GB RAM, 120GB storage | 4vCPU, 16GB RAM, 120GB storage     |
| OS_Master1   | Master (или control plane) | RHCOS           | 4vCPU, 16GB RAM, 120GB storage | 4vCPU, 16GB RAM, 120GB storage     |
| OS_Master2   | Master (или control plane) | RHCOS           | 4vCPU, 16GB RAM, 120GB storage | 4vCPU, 16GB RAM, 120GB storage     |
| OS_Master3   | Master (или control plane) | RHCOS           | 4vCPU, 16GB RAM, 120GB storage | 4vCPU, 16GB RAM, 120GB storage     |
| OS_Worker1   | Compute                    | RHCOS или RHEL* | 2vCPU, 8GB RAM, 120GB storage  | **8vCPU, 32GB RAM, 120GB storage** |
| OS_Worker2   | Compute                    | RHCOS или RHEL* | 2vCPU, 8GB RAM, 120GB storage  | **8vCPU, 32GB RAM, 120GB storage** |

Несколько замечаний по таблице:

1. Виртуалка OS_Bootstrap нужна нам для создания кластера. После ее можно утилизировать. Или утилизировать и использовать освободившиеся ресурсы для чего-то еще - наш случай!
2. Для Compute узлов можно использовать RHEL вместо RHCOS, но это уже, судя по всему, не мейнстримовый вариант, поэтому это не для нас.
3. Как видите, я рекомендую давать для Compute узлов гораздо больше ресурсов . Справедливости ради нужно сказать, что для установки самого кластера OpenShift хватит минимума, но когда мы начнем ставить Cloud Pak, начнется самое интересное. Ресурсов нужно будет очень много! То, что указано - навскидку достаточно, но то же самое поделить на 2 (4vCPU и 16GB RAM) - 100% недостаточно. Даже не пробуйте. Промежуточные варианты не пробовал, но как вы понимаете, можно найти какую-то золотую середину, если ресурсов очень жалко.
4. Для каждой из виртуалок у меня стоит флаг: "Expose hardware-assisted virtualization to the guest operating system" (флаг для VMware, устанавливается в веб-интерфейсе). Не думаю, что это необходимо - честно говоря, не хотелось проверять - но имейте в виду на всякий случай.

У меня получилась приблизительно вот такая картина:

![](images/01.VMs.png)

| OS_Bootstrap                 | OS_MasterX                | OS_WorkerY                |
| ---------------------------- | ------------------------- | ------------------------- |
| ![](images/02.Bootstrap.png) | ![](images/03.Master.png) | ![](images/04.Worker.png) |

Все виртуалки в одной сети. В моем случае 192.168.8.0/24. 

Как видите, у меня все на одном серваке, что не есть правильно, но никто не помешает вам растащить это на нужное количество физических серверов для обеспечения отказоустойчивости.

##### Вспомогательные сервисы

Еще перед установкой нужно подготовить сопутствующую инфраструктуру. Понадобится она нам уже в процессе установки, так что отложить до лучших времен не получится. Если коротко резюмировать, нам нужны:

1. Управляющая машина (с Linux), откуда мы будет отслеживать установку, выполнять административные операции по отношению к кластеру и где будут лежат ключи для доступа на машины, входящие в кластер. Особых требований к этой машине нет: в моем случае она находится в той же самой подсети, что и машины из моего будущего кластера.
2. DNS сервер. Это DNS, который должны будут использовать машины из вашего кластера и клиенты OpenShift. Сами решайте, нужно вам сделать для этого отдельный сервер или использовать уже существующий. Я взял свой, запущенный для других целей, BIND и сделал там поддомен в уже существующей зоне для моих машин из кластера. Мой базовый домен: i.qdts.ru, для кластера я буду использовать **ipa.i.qdts.ru**.
3. Балансировщик нагрузки. Должен уметь работать на уровне TCP, чтобы на нем не терминировался SSL. Вообще, в документации про него написано ни разу не подробно, так что выбор за вами. Мне сначала пришло в голову настроить nginx, но потом я, чутка погуглив, увидел, что все используют для этой цели HAProxy (например вот здесь, хорошая статья, хоть и не на 100% уже актуальная: https://www.openshift.com/blog/openshift-4-bare-metal-install-quickstart)
4. Веб-сервер. Первый попавшийся веб-сервер, куда нужно будет закинуть небольшое количество файлов (около 2-3GB) на время установки. Можно быстро поставить nginx/Apache. У меня уже был под рукой IBM HTTP Server (сделан на основе Apache).

Дальше по каждому пункту подробнее.

###### Управляющая машина

Я использовал машину с CentOS 8 в той же подсети, что и мои будущие ноды кластера (сначала пытался использовать CentOS 6, но там уже протухшие сишные либы, которые не обновить из репозитория). Желательно, чтобы там было по меньшей мере 200GB свободного места в файловой системе. Пригодится, при установке Cloud Pak.

Что нужно сделать на этой машине? 

- Отнести туда 3 файла из скачанных нами ранее, а именно openshift-install-linux.tar.gz, openshift-client-linux.tar.gz, pull-secret.txt.

- Распаковать первые два архива: из них мы получим несколько бинарей, которые пригодятся нам  в процессе установки и позже. Их можно скопировать куда-то в район каталога из PATH:

  ```bash
  [root@dockerhost openshift]$ cd /openshift/   
  [root@dockerhost openshift]$ ls  
  openshift-client-linux.tar.gz openshift-install-linux.tar.gz  pull-secret.txt   
  [root@dockerhost openshift]$ tar xvf openshift-client-linux.tar.gz   
  README.md  
  oc  
  kubectl  
  [root@dockerhost openshift]$ tar xvf openshift-install-linux.tar.gz  
  README.md  
  openshift-install  
  [root@dockerhost openshift]$ cp openshift-install oc kubectl /usr/local/sbin/
  ```

  

- Нужно либо сгенерировать ssh ключ (описано в документации), который будет использоваться для доступа к машинам в кластере, либо воспользоваться уже существующим (мой случай):

  ```bash
  [root@dockerhost openshift]$ ls ~/.ssh  
  authorized_keys  config  id_ed25519  id_ed25519.pub  id_rsa  id_rsa.pub  known_hosts  
  [root@dockerhost openshift]$ cat ~/.ssh/config  
  IdentityFile **/root/.ssh/id_ed25519**
  ```
  
  

###### DNS сервер

Я использовал BIND, запущенный в докере на машине из пункта 1. Тут не принципиально, что именно использовать, главное, чтобы там были нужные записи и все машины из кластера использовали его (могли бы до него добраться, так как его использование мы настроим чуть позже). Ниже приведены выдержки из ключевых конфигов, которые важны для нас:

Прямая зона (/docker-volumes-config/bind/zones/db.i.qdts.ru):

```
...

;for OpenShift  
bootstrap.ipa                    IN      A       192.168.8.240  
;master0.ipa                     IN      A       192.168.8.241  
;master1.ipa                     IN      A       192.168.8.242  
;master2.ipa                     IN      A       192.168.8.243  
worker0.ipa                      IN      A       192.168.8.245  
worker1.ipa                      IN      A       192.168.8.246  
storage.ipa                       IN      A       192.168.8.238  

api.ipa                               IN      A       192.168.8.239  
api-int.ipa                         IN      A       192.168.8.239  
*.apps.ipa                        IN      A       192.168.8.239  
etcd-0.ipa                         IN      A       192.168.8.241  
etcd-1.ipa                         IN      A       192.168.8.242   
etcd-2.ipa                         IN      A       192.168.8.243  

\_etcd-server-ssl.\_tcp.ipa  86400 IN    SRV 0        10     2380 etcd-0.ipa.i.qdts.ru.  
\_etcd-server-ssl.\_tcp.ipa  86400 IN    SRV 0        10     2380 etcd-1.ipa.i.qdts.ru.  
\_etcd-server-ssl.\_tcp.ipa  86400 IN    SRV 0        10     2380 etcd-2.ipa.i.qdts.ru.  

...
```

Обратная зона - тоже обязательно нужна (/docker-volumes-config/bind/zones/db.192.168):

```
...

;for OpenShift  
240.8           IN      PTR     bootstrap.ipa.i.qdts.ru.  
;241.8          IN      PTR     master0.ipa.i.qdts.ru.  
;242.8          IN      PTR     master1.ipa.i.qdts.ru.  
;243.8          IN      PTR     master2.ipa.i.qdts.ru.  
245.8           IN      PTR     worker0.ipa.i.qdts.ru.  
246.8           IN      PTR     worker1.ipa.i.qdts.ru.  

239.8           IN      PTR     api.ipa.i.qdts.ru.  
239.8           IN      PTR     api-int.ipa.i.qdts.ru.  
241.8           IN      PTR     etcd-0.ipa.i.qdts.ru.  
242.8           IN      PTR     etcd-1.ipa.i.qdts.ru.  
243.8           IN      PTR     etcd-2.ipa.i.qdts.ru.  

...
```

Буквально несколько замечаний:

- В первом файле короткие имена, полные будут а-ля bootstrap.ipa.i.qdts.ru
- storage.ipa - пока нам не нужен, но пригодится чуть позже, не стал его убирать
- Самые внимательные заметили, что у меня закомментированы мастера. На самом деле на эти же ip ссылаются обязательные записи etcd-X.ipa.i.qdts.ru. Когда я оставлял и те, и другие записи, это приводило к несмертельным, но не очень приятным последствиям при определение имен узлов в кластере. Я пришел к тому, что мне проще оставить только etcd-X.ipa.i.qdts.ru.

Запускаю свой контейнер с bind приблизительно вот так (Предварительно была сделана сетка std типа bridge 10.10.0.0/16):

```bash
docker run --name bind -d -p 53:53/udp -p 53:53 -v /docker-volumes-data:/var/cache/bind -v /docker-volumes-config/bind:/etc/bind  --network std --ip 10.10.10.10 --restart always ventz/bind
```



###### Балансировщик нагрузки

Как было сказано выше, я решил использовать HAProxy. Также запускаю его в докер контейнере, на той же самой управляющей машине. Сразу можно оговориться, что он будет использоваться как в процессе установки, так и в процессе эксплуатации кластера. Как для административного траффика (API для вызова от одних машин кластера к другим), так и для бизнесового (запросы к нашим приложениям). Следовательно, стратегически можно рассмотреть варианты с несколькими балансировщиками для разных целей. Для начала не будем усложнять.

Вот конфиг (/docker-volumes-config/haproxy/haproxy.cfg):

```
backend openshift-api-server  
 balance source  
 mode tcp   
 server bootstrap 192.168.8.240:6443 check  
 server master0 192.168.8.241:6443 check  
 server master1 192.168.8.242:6443 check  
 server master2 192.168.8.243:6443 check  

frontend machine-config-server  
 bind *:22623  
 default_backend machine-config-server  
 mode tcp  
 option tcplog  

backend machine-config-server  
 balance source  
 mode tcp  
 server bootstrap 192.168.8.240:22623 check  
 server master0 192.168.8.241:22623 check  
 server master1 192.168.8.242:22623 check  
 server master2 192.168.8.243:22623 check  

frontend ingress-http  
 bind *:80  
 default_backend ingress-http  
 mode tcp  
 option tcplog  

backend ingress-http  
 balance source  
 mode tcp  
 server worker0 192.168.8.245:80 check  
 server worker1 192.168.8.246:80 check  

frontend ingress-https  
 bind *:443  
 default_backend ingress-https  
 mode tcp  
 option tcplog  

backend ingress-https  
 balance source  
 mode tcp  
 server worker0 192.168.8.245:443 check  
 server worker1 192.168.8.246:443 check  
```

Думаю, здесь все более-менее понятно, глядя на порты и на машины, куда этот траффик будет перенаправляться.

Запускаю контейнер:

```
docker run -d --name my-running-haproxy -v /docker-volumes-config/haproxy/:/usr/local/etc/haproxy:ro -p 80:80 -p 443:443 -p 6443:6443 -p 22623:22623 --restart always haproxy:1.7
```

Если меняете конфигурацию балансировщика, то можно красиво попросить ее перечитать:

```bash
docker kill -s HUP my-running-haproxy
```

Спойлер: было несколько случаев, когда этот балансировщик портил мне жизнь. В частности, приходилось ловить ошибки EOF при попытках инициализировать некоторые машины из кластера. При этом проблема имела место быть только при работе через балансировщик: при непосредственном взаимодействии с bootstrap машиной через curl все был ок. Лечилось перезагрузкой haproxy.

###### Веб-сервер

Было бы логично и правильно запустить его также в контейнере на той же управляющей машине. Но я так делать не буду, вместо этого воспользуюсь существующим на машине **192.168.8.201**. Прямо в корневой каталог для статического контента (в моем случае **/opt/IBM/HTTPServer/htdocs/**) кладу файл **rhcos-4.3.8-x86_64-metal.x86_64.raw.gz**. Переименовываю его в нечто короткое и лаконичное, например **cos**. Проверьте, что у others есть права на чтение (r) этого файла. Это относится и ко всем остальным файлам, которые мы сюда закинем чуть позже.

Чуть позже на этот веб-сервер нужно будет закинуть еще кое-что, но пока у нас этого кое-чего нет, так что оставим его в покое.

##### Прочие требования

В принципе, я сделал для моего случая все, что нужно, но у вас может возникнуть потребность в настройке вашего firewall. Здесь не буду повторять документацию, просто сошлюсь на нее. Там довольно однозначно написано по нашим кластерным машинам: какие порты они используют и для чего:

https://docs.openshift.com/container-platform/4.3/installing/installing_bare_metal/installing-bare-metal.html#installation-network-user-infra_installing-bare-metal

В итоге получается приблизительно такая картинка (здесь есть моя специфика - показано, какие сервисы совмещены, но я думаю, вы легко экстраполируете на свою ситуацию):

![](images/07.Architecture.JPG)

Ура! Закончили с артподготовкой, пора приступать к установке.

####  Установка OpenShift

##### Генерация конфигурационных файлов

Важный дисклеймер: вид создаваемого конфига зависит от вашего варианта установки (Bare Metal, vSphere, OpenStack...).

Для начала нам нужно сгенерировать конфиги, которые мы будем использовать в процессе установки. Для этого идем на управляющую машину:

```bash
[root@dockerhost /]$ mkdir -p /openshift/install/  
[root@dockerhost openshift]$ cd /openshift/install/  
[root@dockerhost install]$ vi install-config.yaml  
```

install-config.yaml - конфиг, который нам нужно сделать, как вы уже наверное поняли. В качестве образца можно взять листинг из документации, ну или мой (ниже). В документации же есть более подробное, чем у меня, описание отдельных полей. Но давайте посмотрим на мой. Кстати это YAML, так что поаккуратнее с пробелами (мой лучше не копировать - пробелы ведут себя непредсказуемо в .md).

```yaml
apiVersion: v1
baseDomain: i.qdts.ru
compute:
- hyperthreading: Enabled
  name: worker
  replicas: 0
controlPlane:
  hyperthreading: Enabled
  name: master
  replicas: 3
metadata:
  name: ipa
networking:
  clusterNetwork:
  - cidr: 10.128.0.0/16
    hostPrefix: 24
  networkType: OpenShiftSDN
  serviceNetwork:
  - 172.30.0.0/16
platform:
  none: {}
fips: false
pullSecret: '...'
sshKey: 'ssh-ed25519 ...'
```

Жирным выделил то, что вам, вероятно, захочется поменять:

1. baseDomain - здесь должен быть базовый домен для вашего кластера без имени этого самого кластера (то есть, если у вас имя **ipa**, базовый домен **i.qdts.ru**, то итоговое доменное имя для всех служб кластера будет **ipa.i.qdts.ru**).
2. name - собственно имя кластера.
3. cidr - сеть для выделения ip-адресов вашим будущим подам, имеет смысл выбрать нечто не пересекающееся с вашими уже существующими сетями.
4. serviceNetwork - сеть для выделения ip-адресов вашим будущим сервисам, имеет смысл выбрать нечто не пересекающееся с вашими уже существующими сетями.
5. pullSecret - сюда нужно вставить содержимое файла pull-secret.txt, если вы еще не успели про него забыть. Копируете весь файл и вставляете между вот такими ' кавычками.
6. sshKey - сюда нужно скопировать публичный ключ ssh, который мы генерировали/смотрели чуть раньше. В моем случае это файл **id_ed25519.pub** (см. раздел **Управляющая машина**)

Дальше скопируйте файл install-config.yaml куда-нибудь про запас, так как скрипты, которые мы будем запускать, сожрут его с потрохами.

Предполагаем, что install-config.yaml лежит в текущем каталоге.

```bash
openshift-install create manifests --dir=.
```

Дальше открываем один из сгенерированных файлов **./manifests/cluster-scheduler-02-config.yml** и меняем значение свойства **mastersSchedulable** на **False**. После чего запускаем следующую команду:

```bash
openshift-install create ignition-configs --dir=.
```

Ура, теперь в нашем распоряжении есть конфиги для выполнения установки всех узлов нашего кластера:

```bash
[root@dockerhost install]$ pwd  
/openshift/install  
[root@dockerhost install]$ tree  
.  
├── auth  
│   ├── kubeadmin-password  
│   └── kubeconfig  
├── bootstrap.ign  
├── master.ign  
├── metadata.json  
└── worker.ign  
```

**ВАЖНОЕ ЗАМЕЧАНИЕ! В момент генерации этих конфигов создаются, среди всего прочего, сертификаты, срок жизни которых - 24 часа. За эти 24 часа нам нужно успеть доустановить OpenShift (только OpenShift, с Cloud Pak можно не спешить). Я однажды столкнулся с этой проблемой, хотя справедливости ради, в документации об этом предупреждают.**

Далее нужно отнести файлы bootstrap.ign, master.ign, worker.ign на наш веб-сервер. Я положил их в корневой раздел. Переименовал в b.ign, m.ign, w.ign.

Так выглядит корневой каталог моего веб-сервера на данный момент:

```bash
[root@bpmprod htdocs]$ tree  
.  
├── b.ign  
├── cos  
├── m.ign  
└── w.ign   
```



##### Установка RHCOS

Здесь у нас два варианта: либо загружать наши виртуалки с использованием ISO образа, либо настроить PXE сервер для загрузки по сети. Я выбрал первый вариант, хотя и успел об этом потом пожалеть. С PXE больше предварительной работы, еще кое-что нужно докачать к тому, что у нас уже есть, но при этом, если что-то пошло не так - запустить по N-ому разу установку мы сможем гораздо быстрее, чем в моем случае. Но у нас же все должно получиться с первого раза, верно? :)

Начнем с виртуалки OS_Bootstrap. Нужно подключить к ней ISO образ RHCOS (мы скачали его ранее), и заставить загрузиться с виртуального привода, где смонтирован этот ISO.

Когда мы запустим виртуалку, она предложит нажать TAB для указания опций загрузки, так и поступим.

![](images/05.Install-window.png)

Теперь самое мерзкое. Мерзость в том, что интерфейс моего vSphere Client не позволяет копипастить. К уже имеющимся параметрам запуска нам нужно дописать еще несколько:

- coreos.inst.install_dev=sda 
  - вместо sda здесь, возможно, нужно подставить что-то актуальное для вашей системы. Это имя, которое использует ваш Linux по умолчанию для обозначения первого диска, который подсовывает ему систему виртуализации. Можете посмотреть на любую другую ВМ на этом сервере, чтобы проверить. Может быть, там будет vda вместо sda.
- coreos.inst.image_url=http://192.168.8.201/cos
  - указание на raw image, который мы заранее залили на веб-сервер.
- coreos.inst.ignition_url=http://192.168.8.201/b.ign
  - указание на ignition конфиг для виртуалки этого типа, сейчас это bootstrap. Думаю, заполняя это и предыдущее свойства, вы уже догадались, почему я переименовал все файлы на моем веб-сервере :)
- ip=192.168.8.240::192.168.8.99:255.255.255.0:bootstrap.ipa.i.qdts.ru:ens32:none
  - самое очепяточное (:: после первого ip это не ошибка, так должно быть), ip-конфигурация. Для моего примера:
    - 192.168.8.240 - IP той машины, которую мы разливаем
    - 192.168.8.99 - IP маршрутизатора
    - 255.255.255.0 - маска
    - bootstrap.ipa.i.qdts.ru - доменное имя для машины
    - ens32 - название сетевого интерфейса для этой ВМ, у меня они назывались так, в вашем случае может быть и по-другому
    - none - всегда так
- nameserver=192.168.8.239
  - DNS сервер

В итоге получается нечто подобное:

![](images/06.Install-params.png)

Проверьте, что не забыли запустить веб-сервер, что все нужные файлы доступны, и нажимая Enter, запускаем установку. Надеемся, что нас не заругают. Если что-то не так, нам быстро дадут об этом знать. Если все ок, то установка завершится за 2-3 минуты. После установки автоматом запускается перезагрузка, если вы не успеете отмонтировать диск с ISO, то загрузитесь с него. Его нужно будет размонтировать и перезагрузиться еще разок. Когда RHCOS загрузится, появится приглашение для ввода логина/пароля (если будут нон-стоп появляться ошибки SELinux - это ок). У нас их нет, но мы можем попасть на эту виртуалку с управляющей машины:

```
[root@dockerhost install]$ ssh core@192.168.8.240  
The authenticity of host '192.168.8.240 (192.168.8.240)' can't be established.  
ECDSA key fingerprint is SHA256:oZ+Cm9lnUA/irol7N+BXvJBM6ALOz5VmrjI4dzlCeYY.  
Are you sure you want to continue connecting (yes/no)? yes  
Warning: Permanently added '192.168.8.240' (ECDSA) to the list of known hosts.  
Red Hat Enterprise Linux CoreOS 43.81.202003191953.0  
Part of OpenShift 4.3, RHCOS is a Kubernetes native operating system  
managed by the Machine Config Operator (`clusteroperator/machine-config`).  

WARNING: Direct SSH access to machines is not recommended; instead,  
make configuration changes via `machineconfig` objects:  
https://docs.openshift.com/container-platform/4.3/architecture/architecture-rhcos.html

---

This is the bootstrap node; it will be destroyed when the master is fully up.   

The primary service is "bootkube.service". To watch its status, run e.g.  

journalctl -b -f -u bootkube.service  
[core@bootstrap ~]$  
```

Имеет смысл выполнить предлагаемую команду:

```
journalctl -b -f -u bootkube.service
```

Если самое страшное, что мы там увидим за 5 минут, это:

```
May 09 10:45:35 bootstrap.ipa.i.qdts.ru bootkube.sh[1938]: https://etcd-1.ipa.i.qdts.ru:2379 is unhealthy: failed to commit proposal: context deadline exceeded
May 09 10:45:35 bootstrap.ipa.i.qdts.ru bootkube.sh[1938]: https://etcd-0.ipa.i.qdts.ru:2379 is unhealthy: failed to commit proposal: context deadline exceeded
May 09 10:45:35 bootstrap.ipa.i.qdts.ru bootkube.sh[1938]: https://etcd-2.ipa.i.qdts.ru:2379 is unhealthy: failed to commit proposal: context deadline exceeded
May 09 10:45:35 bootstrap.ipa.i.qdts.ru bootkube.sh[1938]: Error: unhealthy cluster
```

...то дело в шляпе! Если же там будет цикл, в котором наш bootstrap пытается (но не может) запустить контейнеры а-ля etcd-signer или etcdctl, то дело - шляпа. Нужно проводить работу над ошибками и переустанавливать bootstrap. Ну или вручную повоевать с этими контейнерами, а-ля:

```bash
sudo podman rm -f etcd-signer
```

Еще можно запустить:

```bash
sudo netstat -tulpn
```

Если кто-то слушает 22623, 6443, то это хороший знак.

Если все ок, то отлично, едем дальше! Дело за малым. Повторить процедуру для 3 master и 2 worker! Чем у них будут отличаться параметры установки:

- iso, который мы монтируем - тот же
- coreos.inst.install_dev - наверняка тот же
- coreos.inst.image_url - тот же
- coreos.inst.iginition_url - для мастеров нужно будет написать .../m.ign а для воркеров .../w.ign
- ip. маршрутизатор, маска, сетевой интерфейс, none, dns - те же, ip и доменные имена у каждого свои
- nameserver - тот же

В какой последовательности это все делать? По моему опыту, лучше строго последовательно:

- прописали параметры для master0
- дождались окончания установки
- перезагрузились
- помониторили состояние машины (journalctl -f)
- дождались еще одной автоматической перезагрузки
- убедились в успешном запуске машины
- приступили к следующей итерации... (после того, как закончите с последним мастером, можно хорошо передохнуть, потому что в этот момент запускается довольно много разной движухи для кластера, и пока она не дойдет до логического завершения, установить worker не получится - я бы подождал минут 30)

Дальше приведены строчки с дополнительными (помимо уже установленных) параметрами установки для каждой из машин (для полноты картины добавил сюда и bootstrap, с которым мы уже разобрались). Кстати, зачем для worker прописывать nameserver в двух местах? Не знаю, может быть уже и не нужно, но я в свое время немного пострадал, не сделав так.

| Виртуалка    | Параметры установки                                          |
| ------------ | ------------------------------------------------------------ |
| OS_Bootstrap | coreos.inst.install_dev=sda coreos.inst.image_url=http://192.168.8.201/cos coreos.inst.iginition_url=http://192.168.8.201/b.ign ip=192.168.8.240::192.168.8.99:255.255.255.0:bootstrap.ipa.i.qdts.ru:ens32:none nameserver=192.168.8.239 |
| OS_Master1   | coreos.inst.install_dev=sda coreos.inst.image_url=http://192.168.8.201/cos coreos.inst.iginition_url=http://192.168.8.201/m.ign ip=192.168.8.241::192.168.8.99:255.255.255.0:master0.ipa.i.qdts.ru:ens32:none nameserver=192.168.8.239 |
| OS_Master2   | coreos.inst.install_dev=sda coreos.inst.image_url=http://192.168.8.201/cos coreos.inst.iginition_url=http://192.168.8.201/m.ign ip=192.168.8.242::192.168.8.99:255.255.255.0:master1.ipa.i.qdts.ru:ens32:none nameserver=192.168.8.239 |
| OS_Master3   | coreos.inst.install_dev=sda coreos.inst.image_url=http://192.168.8.201/cos coreos.inst.iginition_url=http://192.168.8.201/m.ign ip=192.168.8.243::192.168.8.99:255.255.255.0:master2.ipa.i.qdts.ru:ens32:none nameserver=192.168.8.239 |
| OS_Worker1   | coreos.inst.install_dev=sda coreos.inst.image_url=http://192.168.8.201/cos coreos.inst.iginition_url=http://192.168.8.201/w.ign ip=192.168.8.245::192.168.8.99:255.255.255.0:worker0.ipa.i.qdts.ru:ens32:none:**192.168.8.239** nameserver=192.168.8.239 |
| OS_Worker2   | coreos.inst.install_dev=sda coreos.inst.image_url=http://192.168.8.201/cos coreos.inst.iginition_url=http://192.168.8.201/w.ign ip=192.168.8.246::192.168.8.99:255.255.255.0:worker1.ipa.i.qdts.ru:ens32:none:**192.168.8.239** nameserver=192.168.8.239 |

Можете посетовать на меня за то, что разъехались номера виртуалок, хостнеймов и айпишников - самому больно.

Между тем, уже можно попробовать на управляющей машине посмотреть, как поживает наш кластер:

```bash
[root@dockerhost install]$ export KUBECONFIG=/openshift/4.3/auth/kubeconfig

[root@dockerhost install]$ oc get nodes
NAME                   STATUS   ROLES    AGE     VERSION
etcd-0.ipa.i.qdts.ru   Ready    master   3m45s   v1.16.2
etcd-1.ipa.i.qdts.ru   Ready    master   3m46s   v1.16.2
etcd-2.ipa.i.qdts.ru   Ready    master   3m23s   v1.16.2

[root@dockerhost install]$ openshift-install --dir=. wait-for bootstrap-complete --log-level=info

INFO Waiting up to 30m0s for the Kubernetes API at https://api.ipa.i.qdts.ru:6443...
INFO API v1.16.2 up
INFO Waiting up to 30m0s for bootstrapping to complete...
INFO It is now safe to remove the bootstrap resources
```

Это хороший знак! Значит, bootstrap машину можно выводить из балансировки нагрузки! В haproxy сделать вот так:

```
...

backend openshift-api-server  
balance source  
mode tcp   
#server bootstrap 192.168.8.240:6443 check  
server master0 192.168.8.241:6443 check  
server master1 192.168.8.242:6443 check  
server master2 192.168.8.243:6443 check  

frontend machine-config-server  
bind *:22623  
default_backend machine-config-server  
mode tcp  
option tcplog  

backend machine-config-server  
balance source  
mode tcp  
#server bootstrap 192.168.8.240:22623 check 
server master0 192.168.8.241:22623 check  
server master1 192.168.8.242:22623 check  
server master2 192.168.8.243:22623 check  

...
```

Дальше нужно еще пару раз (с небольшим интервалом времени) подтвердить все Pending сертификаты для worker узлов: процесс показан ниже. Сначала первая пачка, потом вторая.

> **[root@dockerhost install]$ oc get csr**  
> NAME        AGE     REQUESTOR                                                                   CONDITION  
> **csr-257rb   12m     system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Pending**  
> csr-5rdwn   20m     system:node:etcd-0.ipa.i.qdts.ru                                            Approved,Issued  
> csr-69vvm   20m     system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-c8b7r   20m     system:node:etcd-1.ipa.i.qdts.ru                                            Approved,Issued  
> csr-gh72q   20m     system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-llclb   20m     system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> **csr-pk6wh   3m36s   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Pending**  
> csr-v8hp9   20m     system:node:etcd-2.ipa.i.qdts.ru                                            Approved,Issued  
> **[root@dockerhost install]$ oc adm certificate approve csr-257rb csr-pk6wh**  
> certificatesigningrequest.certificates.k8s.io/csr-257rb approved  
> certificatesigningrequest.certificates.k8s.io/csr-pk6wh approved  
> **[root@dockerhost install]$ oc get csr**  
> NAME        AGE    REQUESTOR                                                                   CONDITION  
> csr-257rb   15m    system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-5rdwn   23m    system:node:etcd-0.ipa.i.qdts.ru                                            Approved,Issued  
> csr-69vvm   23m    system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-c8b7r   23m    system:node:etcd-1.ipa.i.qdts.ru                                            Approved,Issued  
> csr-gh72q   23m    system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-llclb   23m    system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-pk6wh   6m5s   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> **csr-rjrsn   81s    system:node:worker0.ipa.i.qdts.ru                                           Pending**  
> csr-v8hp9   22m    system:node:etcd-2.ipa.i.qdts.ru                                            Approved,Issued  
> **csr-vgz6f   85s    system:node:worker1.ipa.i.qdts.ru                                           Pending**  
> [root@dockerhost install]$  
> **[root@dockerhost install]$ oc adm certificate approve csr-rjrsn csr-vgz6f**  
> certificatesigningrequest.certificates.k8s.io/csr-rjrsn approved  
> certificatesigningrequest.certificates.k8s.io/csr-vgz6f approved  
> [root@dockerhost install]$  
> **[root@dockerhost install]$ oc get csr**  
> NAME        AGE     REQUESTOR                                                                   CONDITION  
> csr-257rb   15m     system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-5rdwn   23m     system:node:etcd-0.ipa.i.qdts.ru                                            Approved,Issued  
> csr-69vvm   23m     system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-c8b7r   23m     system:node:etcd-1.ipa.i.qdts.ru                                            Approved,Issued  
> csr-gh72q   23m     system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-llclb   23m     system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-pk6wh   6m27s   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued  
> csr-rjrsn   103s    system:node:worker0.ipa.i.qdts.ru                                           Approved,Issued  
> csr-v8hp9   23m     system:node:etcd-2.ipa.i.qdts.ru                                            Approved,Issued  
> csr-vgz6f   107s    system:node:worker1.ipa.i.qdts.ru                                           Approved,Issued  
> [root@dockerhost install]$  
> **[root@dockerhost install]$ oc get nodes**  
> NAME                    STATUS     ROLES    AGE     VERSION  
> etcd-0.ipa.i.qdts.ru    Ready      master   24m     v1.16.2  
> etcd-1.ipa.i.qdts.ru    Ready      master   24m     v1.16.2  
> etcd-2.ipa.i.qdts.ru    Ready      master   23m     v1.16.2  
> *worker0.ipa.i.qdts.ru   NotReady   worker   2m17s   v1.16.2  
> worker1.ipa.i.qdts.ru   NotReady   worker   2m20s   v1.16.2*  
> **[root@dockerhost install]$ oc get nodes**  
> NAME                    STATUS   ROLES    AGE     VERSION  
> etcd-0.ipa.i.qdts.ru    Ready    master   27m     v1.16.2  
> etcd-1.ipa.i.qdts.ru    Ready    master   27m     v1.16.2  
> etcd-2.ipa.i.qdts.ru    Ready    master   27m     v1.16.2  
> *worker0.ipa.i.qdts.ru   Ready    worker   5m48s   v1.16.2  
> worker1.ipa.i.qdts.ru   Ready    worker   5m51s   v1.16.2*

Теперь у нас есть кластер на 5 узлов!

#### Базовая настройка OpenShift

Можем ли мы наконец начать пользовать нашим кластером и устанавливать туда Cloud Pak? Конечно, нет - это было бы слишком просто!

Если посмотреть документацию, нам советуют понять состояние кластерных операторов (строительные кирпичи OpenShift, которые собственно и обеспечивают его функциональность):

```bash
[root@dockerhost install]$ oc get clusteroperators
NAME                                       VERSION   AVAILABLE   PROGRESSING   DEGRADED   SINCE  
authentication                        4.3.19        True              False                    False               7m
...
```

После того, как вы все поставили, подождали минут 15-20, все сервисы должны быть в статусе AVAILABLE (как у меня выше), никаких PROGRESSING или DEGRADED. Если у вас именно так - это хорошо, но обольщаться рано: проблема в том, что нам нужно настроить хранилище для образов наших контейнеров (image registry), которое по факту еще работает. Что нужно для хранилища? Объект типа Storage Class (если кто не в курсе: описание некоего провайдера персистентного хранилища, обычно в виде сетевой файловой системы или сетевого же блочного устройства). Подробнее о том, что поддерживается, можно почитать на странице документации Kubernetes https://kubernetes.io/docs/concepts/storage/storage-classes/#quobyte

Я для своего эксперимента буду использовать решение, лежащее на поверхности - NFS. Рекомендовать вам его для прода не могу, так как даже в моих не самых глубоких изысканиях я столкнулся с определенными проблемами. При этом есть и еще один нюанс: какие-то системы из нашего Cloud Pak хотят Storage Class файлового типа (NFS, Gluster, CephFS и т.п.) - MQ, App Connect, а какие-то блочного (RBD, iSCSI и т.п.) - API, Operations Dashboard. Так что стратегически нам понадобится и то, и другое.

Где бы создать такое хранилище? Под рукой есть bootstrap машина, которая нам больше не нужна. Я на ее месте сделаю новую виртуалку с CentOS 8, откуда буду раздавать NFS. Если у вас не все так плохо с дисковыми ресурсами, bootstrap можно оставить на всякий случай.

Итак. На виртуалке с NFS ситуация дело обстоит вот так:

```bash
[root@storage ~]$ lvdisplay /dev/storage_vg/storage_lv  
--- Logical volume ---  
LV Path                /dev/storage_vg/storage_lv  
LV Name                storage_lv  
VG Name                storage_vg  
...  
LV Size                122.00 GiB  
...  

[root@storage ~]$ mount  
...  
/dev/mapper/storage_vg-storage_lv on /storage type xfs (rw,relatime,attr2,inode64,noquota)  
...  
[root@storage ~]$ cat /etc/exports  
/storage 192.168.8.0/24(rw,sync,no_wdelay,no_root_squash,insecure,fsid=0)  
```

Все это уже экспортировано, что можно проверить с подходящей машины с помощью

```bash
mount storage.ip.i.qdts.ru:/storage /mnt
```

Наша следующая задача - заточить наш OpenShift на использование этой шары. Для этого нужна дока: https://github.com/kubernetes-incubator/external-storage/tree/master/nfs-client Тут все довольно подробно и аккуратно написано, поэтому не буду пересказывать. Короткое резюме: создаем объект provisioner в проекте default (NAMESPACE=default), разбираемся с правами, делаем файл deployment.yaml содержания а-ля...
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nfs-client-provisioner
  labels:
    app: nfs-client-provisioner
  # replace with namespace where provisioner is deployed
  namespace: default
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nfs-client-provisioner
  template:
    metadata:
      labels:
        app: nfs-client-provisioner
    spec:
      serviceAccountName: nfs-client-provisioner
      containers:
        - name: nfs-client-provisioner
          image: quay.io/external_storage/nfs-client-provisioner:latest
          volumeMounts:
            - name: nfs-client-root
              mountPath: /persistentvolumes
          env:
            - name: PROVISIONER_NAME
              value: fuseim.pri/ifs
            - name: NFS_SERVER
              value: storage.ipa.i.qdts.ru
            - name: NFS_PATH
              value: /storage
      volumes:
        - name: nfs-client-root
          nfs:
            server: storage.ipa.i.qdts.ru
            path: storage
```

...применяем его, делаем storage class а-ля...

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: managed-nfs-storage
provisioner: fuseim.pri/ifs # or choose another name, must match deployment's env PROVISIONER_NAME'
parameters:
  archiveOnDelete: "false"
```

...проверяем. Весь процесс в общих чертах описан ниже:

```bash
[root@dockerhost 4.3]$ mkdir storage
[root@dockerhost 4.3]$ cd storage/
[root@dockerhost storage]$ git clone https://github.com/kubernetes-incubator/external-storage.git
Cloning into 'external-storage'...
...
[root@dockerhost storage]$ cd external-storage/nfs-client/
[root@dockerhost nfs-client]$ NAMESPACE=default
[root@dockerhost nfs-client]$ sed -i'' "s/namespace:.*/namespace: $NAMESPACE/g" ./deploy/rbac.yaml
[root@dockerhost nfs-client]$ oc create -f deploy/rbac.yaml
serviceaccount/nfs-client-provisioner created
clusterrole.rbac.authorization.k8s.io/nfs-client-provisioner-runner created
clusterrolebinding.rbac.authorization.k8s.io/run-nfs-client-provisioner created
role.rbac.authorization.k8s.io/leader-locking-nfs-client-provisioner created
rolebinding.rbac.authorization.k8s.io/leader-locking-nfs-client-provisioner created
[root@dockerhost nfs-client]$ oc adm policy add-scc-to-user hostmount-anyuid system:serviceaccount:$NAMESPACE:nfs-client-provisioner
securitycontextconstraints.security.openshift.io/hostmount-anyuid added to: ["system:serviceaccount:default:nfs-client-provisioner"]
[root@dockerhost nfs-client]$ vi deploy/deployment.yaml
...
[root@dockerhost nfs-client]$ oc apply -f deploy/deployment.yaml
deployment.apps/nfs-client-provisioner created
[root@dockerhost nfs-client]$ vi deploy/class.yaml
...
[root@dockerhost nfs-client]$ oc apply -f deploy/class.yaml
storageclass.storage.k8s.io/managed-nfs-storage created
[root@dockerhost nfs-client]$ kubectl create -f deploy/test-claim.yaml -f deploy/test-pod.yaml
persistentvolumeclaim/test-claim created
pod/test-pod created
[root@dockerhost nfs-client]$ kubectl delete -f deploy/test-pod.yaml -f deploy/test-claim.yaml
pod "test-pod" deleted
persistentvolumeclaim "test-claim" deleted
```

Теперь можно было настроить хранилище для нашего image-registry, но я бы не стал делать, как в доках - будет слишком много сложностей. На вашем месте я бы сначала вручную создал бы Persistent Volume Claim:

```bash
[root@dockerhost storage]$ cat pvc.yaml
```

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: image-registry-storage
  namespace: openshift-image-registry
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 70Gi
  storageClassName: managed-nfs-storage
  volumeMode: Filesystem
```

```bash
[root@dockerhost storage]$ oc apply -f pvc.yaml
```

А теперь можно делать примерно то, что написано в документации:

```bash
[root@dockerhost storage]$ oc edit configs.imageregistry.operator.openshift.io
```

И редактируем пару блоков из этой спецификации:

```yaml
...
spec:
  defaultRoute: false
  disableRedirect: false
  httpSecret: b66b393a502da478456113bbd022e320aa3f0a5f3e473b41b0255b7a5f9890100c12a8c967ff452e493c241a689665f67b2553d2f698bed1dc95bf27bd7f41e5
  logging: 2
  managementState: Managed # вот здесь мы поменяли Removed на Managed
  proxy:
    http: ""
    https: ""
    noProxy: ""
  readOnly: false
  replicas: 1
  requests:
    read:
      maxInQueue: 0
      maxRunning: 0
      maxWaitInQueue: 0s
    write:
      maxInQueue: 0
      maxRunning: 0
      maxWaitInQueue: 0s
  storage: # а вот здесь написали две строчки ниже, причем указали имя созданной pvc!
    pvc:
      claim: image-registry-storage
  ...
```

После того, как мы сохраним изменения, нужно подождать минут 5-10 и убедиться в...

```bash
[root@dockerhost storage]$ oc get clusteroperator image-registry
NAME             VERSION   AVAILABLE   PROGRESSING   DEGRADED   SINCE
image-registry   4.3.19    True        False         False      38m
```

Если же там PROGRESSING или DEGRADED, то это проблема, которую нужно решать.

Теперь - последний, решающий шаг. На нашей управляющей машине запускаем:

```bash
[root@dockerhost install]$ openshift-install --dir=. wait-for install-complete
INFO Waiting up to 30m0s for the cluster at https://api.ipa.i.qdts.ru:6443 to initialize...
INFO Waiting up to 10m0s for the openshift-console route to be created...
INFO Install complete!
INFO To access the cluster as the system:admin user when using 'oc', run 'export KUBECONFIG=/openshift/install/4.3/auth/kubeconfig'
INFO Access the OpenShift web-console here: https://console-openshift-console.apps.ipa.i.qdts.ru
INFO Login to the console with user: kubeadmin, password: 9TQqN-TquCo-...
```

Кажется, победили! Но теперь этот кластер не стоит кантовать (в том числе и выключать в ближайшие 24 часа). Это не шутка :) Про это даже в документации написано. Я один раз неприятно пострадал от этого.

Еще советую куда-то в сторонку отнести наш installation_dir и надежно сохранить. Там есть пароль пользователя kubeadmin (в моем случае /openshift/install/auth/kubeadmin-password) и важный конфигурационный файл kubeconfig (/openshift/install/auth/kubeconfig) - все это лучше не терять.

После того, как кластер отстоится сутки, имеет смысл сделать снэпшоты на уровне системы виртуализации/стораджа для всех ваших виртуалок.

### Установка Cloud Pak for Integration

#### Предварительные требования

Теперь давайте ставить Cloud Pak. Не беспокойтесь, тут все чуть попроще, чем с OpenShift. По традиции, ссылочка на доку https://www.ibm.com/support/knowledgecenter/SSGT7J_20.1/install/install.html

У нас есть два варианта развития событий:

1. Использование offline дистрибутива (...using downloaded packages). Все части Cloud Pak скачиваются заранее, поэтому после инсталляции, когда вам захочется MQ, API Connect и прочие DataPower, вам уже не нужно будет ничего докачивать, все нужные дистры будут заложены в вашу установку. Хороший вариант для тех, у кого в окружении плохо обстоит дело с интернетом. При этом, очевидно, нужно довольно много дисковых ресурсов (подробности ниже).
2. Использование online дистрибутива (...using the IBM Entitled Registry). Вам нужен относительно небольшой дистрибутив самого Cloud Pak, но при необходимости развернуть MQ и прочее, эти дистрибутивы будут подкачиваться в онлайн режиме из так называемого IBM Entitled Registry (https://myibm.ibm.com/products-services/containerlibrary). Для того, чтобы им пользоваться, соответствующие продукты должны быть куплены и доступны для вашей учетной записи (IBM id) в системе Passport Advantage. У меня с этим есть определенные сложности, поэтому я выбираю первый вариант.

И в первом, и во втором случае придется скачивать один из дистрибутивов Cloud Pak, который вы решили использовать, с того ресурса, которым вы обычно для этого пользуетесь (Passport Advantage или Software Access Catalog).

Процедуру установки мы будем запускать с управляющей машины, где должен быть установлен docker, и где должно быть чертовски много дисковых ресурсов (если вы не планируете слишком долго плясать с бубном). 200ГБ должно хватить. У меня столько нет, поэтому я плясал-таки с бубном, но не буду вас утомлять подробностями. 

#### Процедура установки

На управляющей машине нужно распаковать дистрибутив и закачать в репозиторий докера служебные образы, которые понадобятся для работы нашего Cloud Pak: 

```bash
tar xvf ibm-cp-int-2020.1.1-offline.tar.gz
tar xf installer_files/cluster/images/common-services-boeblingen-2002-x86_64.tar.gz -O|docker load
```

По итогам выполнения первой команды из архива на ~40GB появился каталог на те же самые ~40GB.  Для выполнения второй команды в момент кульминации нужно будет ~50GB дискового пространства.

Теперь нужно скопировать в правильное место нашего дистрибутива файл kubeconfig от нашей установки OpenShift:

```bash
cd installer_files/cluster/
cp /oopenshift/install/auth/kubeconfig .
```

Дальше чуть отредактировать файл config.yaml:

```
vi config.yaml

...

# указываем узлы кластера, где вы собираетесь раскатать Cloud Pak
cluster_nodes:
  master:
    - worker0.ipa.i.qdts.ru
    - worker1.ipa.i.qdts.ru
  proxy:
    - worker0.ipa.i.qdts.ru
    - worker1.ipa.i.qdts.ru
  management:
    - worker0.ipa.i.qdts.ru
    - worker1.ipa.i.qdts.ru
...

#  указываем наш storage class, сделанный ранее
storage_class: managed-nfs-storage
...

# указываем пароль пользователя admin и регулярку для паролей
default_admin_password:
  blablabla 
password_rules:

  - '(.*)'
    ...

# еще лично я закомментировал пару компонентов Cloud Pak, которые не
# хочу ставить: API и Events. Вряд ли это нужно будет делать вам.
archive_addons:
  icp4i:
    namespace: integration
    repo: local-charts
    path: icp4icontent/IBM-Cloud-Pak-for-Integration-3.1.0.tgz

    charts:
      - name: ibm-icp4i-prod
        values: {}

  mq:
    namespace: mq
    repo: local-charts
    path: icp4icontent/IBM-MQ-Advanced-for-IBM-Cloud-Pak-for-Integration-6.0.0.tgz

  ace:
    namespace: ace
    repo: local-charts
    path: icp4icontent/IBM-App-Connect-Enterprise-for-IBM-Cloud-Pak-for-Integration-3.1.0.tgz

#  eventstreams:
#    namespace: eventstreams
#    repo: local-charts
#    path: icp4icontent/IBM-Event-Streams-for-IBM-Cloud-Pak-for-Integration-1.4.1.x86.pak.tar.gz

#  apic:
#    namespace: apic
#    repo: local-charts
#    path: icp4icontent/IBM-API-Connect-Enterprise-for-IBM-Cloud-Pak-for-Integration-1.0.5.tgz

  aspera:
    namespace: aspera
    repo: local-charts
    path: icp4icontent/IBM-Aspera-High-Speed-Transfer-Server-for-IBM-Cloud-Pak-for-Integration-1.3.0.tgz

  datapower:
    namespace: datapower
    repo: local-charts
    path: icp4icontent/IBM-DataPower-Virtual-Edition-for-IBM-Cloud-Pak-for-Integration-1.0.6.tgz

  assetrepo:
    namespace: integration
    repo: local-charts
    path: icp4icontent/IBM-Cloud-Pak-for-Integration-Asset-Repository-4.0.0.tgz

  tracing:
    namespace: tracing
    repo: local-charts
    path: icp4icontent/IBM-Cloud-Pak-for-Integration-Operations-Dashboard-1.0.2.tgz


```

Теперь запускаем по документации установку, но перед ней я на всякий случай выполню команду для логина (были прецеденты, когда без этого у меня ничего не выходило):

```bash
[root@dockerhost cluster]$ oc login
The server uses a certificate signed by an unknown authority.
You can bypass the certificate check, but any data you send to the server could be intercepted by others.
Use insecure connections? (y/n): y

Authentication required for https://api.ipa.i.qdts.ru:6443 (openshift)
Username: kubeadmin
Password:
Login successful.

You have access to 53 projects, the list has been suppressed. You can list all projects with 'oc projects'

Using project "default".

[root@dockerhost cluster]S sudo docker run -t --net=host -e LICENSE=accept -v $(pwd):/installer/cluster:z -v /var/run:/var/run:z -v /etc/docker:/etc/docker:z --security-opt label:disable ibmcom/icp-inception-amd64:3.2.4 addon
```

Во время установки на управляющей машине понадобится дополнительное пространство (порядка 40GB должно хватить, хотя не скажу на 100%, я ведь не случайно комментировал API и Events). Установка выполняется с помощью Ansible, который запускается в docker контейнере. Занимает порядка двух часов. Не волнуйтесь, если вдруг у вас определенные таски ругаются на невыполненность и пытаются выполниться еще несколько раз в цикле - это фича :) Если у вас все в порядке с ресурсами, то проблем быть не должно. Если же установка сломалась, то нужно зайти в веб-интерфейс OpenShift и посмотреть детали проблемы там.

### Создание инстанса MQ

Дело за малым: запустить то, ради чего мы все это затевали. Не могу сказать, что все продукты (в терминологии Cloud Pak, MQ, Datapower и прочие называются capabilities) разворачиваются одинаково, но по крайней мере интерфейс для этого есть общий. MQ берем просто в качестве примера, с учетом того, что он всеми нежно любим.

#### MQ

Идем в интерфейс Platform Navigator: https://ibm-icp4i-prod-integration.apps.ipa.i.qdts.ru/ В вашем случае, разумеется, домен будет отличаться. Там указываем логин **admin** и пароль, который придумали перед установкой CP и внесли в файл config.yaml.

Там сразу берем быка за рога и говорим, что нам нужен новый instance MQ:

![](images/cp/1.png)

Там рекомендуют посмотреть на пререквизиты для запуска, но у нас все схвачено. Namespace с нужными ролями безопасности и секрет для работы с image-registry для нас уже сделали. Нужный (File) Storage Class у нас тоже есть.

Дальше нужно ответить на небольшое количество вопросов. Там их довольно много, все показывать не буду, сосредоточусь на том, что обязательно нужно заполнить:

```
Helm release name: qm1 (на ваше усмотрение)
Target namespace: mq (лучше использовать его, так как там уже готовы нужные для нас объекты)
Target cluster: local-cluster (здесь вариантов не будет)
```

![](images/cp/2.png)



```
Image repository: image-registry.openshift-image-registry.svc:5000/mq/ibm-mqadvanced-server-integration (для меня было большой загадкой, что и как писать здесь для работы с локальным репозиторием, но на выручку пришла команда oc get images, которая возвращает красивый список образов, в том числе из нашего реестра)
Image pull secret: default-docker-blablabla (название нужно скопировать из списка секретов, что уже есть в этом рабочем пространстве)
Registration image repository: image-registry.openshift-image-registry.svc:5000/mq/ibm-mq-oidc-registration
Cluster hostname: qm1 (на ваше усмотрение)
```

![](images/cp/3.png)

![](images/cp/4.png)

```
Storage class name: managed-nfs-storage (название вашего чуть ранее созданного файлового storage class, для красивых инсталляций есть возможность использовать отдельные классы для данных и логов, но нам это не нужно)
Queue manager name: qm1 (на ваше усмотрение)
```

![](images/cp/5.png)

![](images/cp/6.png)

Нажимаем Install, и если нигде не напортачили, минут через 5 в нашем распоряжении будет полноценный менеджер очередей. Его самочувствие можно проверить в консоли OpenShift. Если у вас есть похожий под в правильном состоянии, это здорово:



![](images/cp/7.png)

Дальше уже можно заскочить в веб-консоль MQ через интерфейс Platform Navigator, чтобы посмотреть, как там обстоят дела с очередями и прочим:

![](images/cp/8.png)

Но это уже совсем другая история! А нашу на этом пора заканчивать)
